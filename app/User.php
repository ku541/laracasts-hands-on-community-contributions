<?php

namespace App;

use App\CommunityLink;
use App\CommunityLinkVote;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Determine if the user user is trusted.
     *
     * @return bool
     */
    public function isTrusted()
    {
        return !! $this->trusted;
    }

    /**
     * Fetch all link that the user has voted up.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function votes()
    {
        return $this->belongsToMany(
            CommunityLink::class, 'community_links_votes'
        )->withTimestamps();
    }

    /**
     * Toggle a vote for a given link.
     *
     * @param  CommunityLink  $communityLink
     */
    public function toggleVoteFor(CommunityLink $communityLink)
    {
        CommunityLinkVote::firstOrNew([
            'user_id'           => $this->id,
            'community_link_id' => $communityLink->id
        ])->toggle();
    }

    /**
     * Determine if the user voted for a given link.
     *
     * @param  CommunityLink  $communityLink
     * @return bool
     */
    public function votedFor(CommunityLink $communityLink)
    {
        return $this->votes->contains($communityLink);
    }
}
