<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Queries\CommunityLinksQuery;
use App\Http\Requests\CommunityLinkForm;
use App\Exceptions\CommunityLinkAlreadySubmitted;

class CommunityLinksController extends Controller
{
    /**
     * Show all community links.
     *
     * @param  Channnel  $channel
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index(Channel $channel)
    {
        $channels = Channel::orderBy('title')->get();
        $paginatedCommunityLinks = (new CommunityLinksQuery())->get(
            request()->exists('popular'), $channel
        );

        return view('community.index', compact(
            'channel',
            'channels',
            'paginatedCommunityLinks'
        ));
    }

    /**
     * Publish a new community link.
     *
     * @param  CommunityLinkForm  $form
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CommunityLinkForm $form)
    {
        try {
            $form->persist();

            if (auth()->user()->isTrusted()) {
                session()->flash('success', 'Thanks for the contribution.');
            } else {
                session()->flash(
                    'success',
                    'Thanks! this contribution will be approved shortly.'
                );
            }
        } catch (CommunityLinkAlreadySubmitted $e) {
            session()->flash(
                'success',
                'That link has already been submitted. Brought it to the top.'
            );
        }

        return redirect()->back();
    }
}
