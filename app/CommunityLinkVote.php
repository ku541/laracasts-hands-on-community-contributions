<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommunityLinkVote extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'community_links_votes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'community_link_id'
    ];

    /**
     * Toggle the existence of the row.
     *
     * @return mixed
     */
    public function toggle()
    {
        if ($this->exists) {
            return $this->delete();
        }

        return $this->save();
    }
}
