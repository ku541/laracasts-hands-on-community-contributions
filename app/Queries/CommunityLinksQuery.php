<?php

namespace App\Queries;

use App\Channel;
use App\CommunityLink;

class CommunityLinksQuery
{
    /**
     * Fetch all relevant community links.
     *
     * @param  bool   $sortByPopular
     * @param  Channel $channel
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function get($sortByPopular, Channel $channel)
    {
        $orderBy = $sortByPopular ? 'votes_count' : 'updated_at';

        return CommunityLink::with('creator', 'channel')
            ->withCount('votes')
            ->forChannel($channel)
            ->where('approved', 1)
            ->orderBy($orderBy, 'desc')
            ->paginate(25);
    }
}
