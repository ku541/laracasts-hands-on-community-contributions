@auth
    <div class="col-md-4">
        <h3>Contribute a Link</h3>

        <div class="panel panel-default">
            <div class="panel-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form method="POST" action="/community">
                    @csrf

                    <div class="form-group">
                        <label for="channel_id">Channel:</label>
                        <select name="channel_id" id="channel_id" class="form-control">
                            <option selected disabled>Pick a Channel...</option>

                            @foreach ($channels as $channel)
                                <option
                                    value="{{ $channel->id }}"
                                    {{ old('channel_id') == $channel->id ? 'selected' : '' }}
                                >
                                    {{ $channel->title }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="title">Title:</label>
                        <input
                            id="title"
                            type="text"
                            name="title"
                            value="{{ old('title') }}"
                            class="form-control"
                            placeholder="What is the title of your article?"
                            required
                        >
                    </div>

                    <div class="form-group">
                        <label for="link">Link:</label>
                        <input
                            id="link"
                            type="url"
                            name="link"
                            value="{{ old('link') }}"
                            class="form-control"
                            placeholder="What is the URL?"
                            required
                        >
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Contribute a Link</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endauth
