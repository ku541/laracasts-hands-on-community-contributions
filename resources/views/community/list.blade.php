<ul class="list-group">
    @if (count($paginatedCommunityLinks))
        @foreach ($paginatedCommunityLinks as $communityLink)
            <li class="list-group-item CommunityLink">
                <form method="POST" action="/votes/{{ $communityLink->id }}">
                    @csrf

                    <button
                        type="submit"
                        class="btn {{ auth()->check() && auth()->user()->votedFor($communityLink) ? 'btn-success' : 'btn-primary' }}"
                        {{ auth()->guest() ? 'disabled' : '' }}
                    >
                        {{ $communityLink->votes->count() }}
                    </button>
                </form>

                @if (auth()->check() && auth()->user()->votedFor($communityLink))
                    +1
                @endif

                <a
                    href="/community/{{ $communityLink->channel->slug }}"
                    class="badge badge-info"
                    style="background-color: {{ $communityLink->channel->color }}"
                >
                    {{ $communityLink->channel->title }}
                </a>

                <a href="{{ $communityLink->link }}" target="_blank">
                    {{ $communityLink->title }}
                </a>

                <small>
                    Contributed by: <a href="#">{{ $communityLink->creator->name }}</a>
                    {{ $communityLink->updated_at->diffForHumans() }}
                </small>
            </li>
        @endforeach
    @else
        <li class="Links__link">
            No contributions yet.
        </li>
    @endif
</ul>

<br>

{{ $paginatedCommunityLinks->appends(request()->query())->links() }}
